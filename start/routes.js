'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Hash = use('Hash')

Route.get('getPass', ({
  response
}) => {
  let pass = Hash.make('root')
  return pass
})
Route.on('/').render('welcome')
Route.group(() => {
  // Login Controller
  Route.post('login', 'API/v1/Login/IndexController.login')
    .validator(['API/Login/Index'])

  // Token Controller
  Route.get('checkToken', 'API/v1/Token/IndexController.checkToken')
    .middleware(['auth'])

  // Penerimaan Material PO Controller
  Route.post('checkCountPenerimaanMaterialPo', 'API/v1/PenerimaanMaterialPo/IndexController.checkCountPenerimaanMaterial')
    .middleware(['auth'])
  Route.post('saveDataDetailPenerimaanMaterialPo', 'API/v1/PenerimaanMaterialPo/IndexController.saveDataDetailMaterialPo')
    .middleware(['auth'])

  // MVIR Controller
  Route.get('mvir/get/allData', 'API/v1/MVIR/IndexController.getAllMvir')
    .middleware(['auth'])
  Route.post('mvir/post/new', 'API/v1/MVIR/IndexController.saveNewMvir')
    .middleware(['auth'])
  Route.get('mvir/get/getDataMaterialByMvir/:idMvir', 'API/v1/MVIR/IndexController.getDataMaterialByMvir')
    .middleware(['auth'])
  Route.post('updateDataMaterialByMvir/', 'API/v1/MVIR/IndexController.updateDataMaterial')
    .middleware(['auth'])

  // Penerimaan Material Customer Controller
  Route.post('penereimaanMaterialCus/save', 'API/v1/PenerimaanMaterialCus/IndexController.savePenerimaanMaterialCus')
    .middleware(['auth'])
  Route.post('penerimaanMaterialCus/get', 'API/v1/PenerimaanMaterialCus/IndexController.getPenerimaanMaterialCus')
    .middleware(['auth'])
  Route.post('saveDataDetalMaterialCus', 'API/v1/PenerimaanMaterialCus/IndexController.saveDataDetailMaterialCus')
    .middleware(['auth'])

  // Pengeluaran Material Controller
  Route.post('getMaterialFromKodeProject', 'API/v1/PengeluaranMaterial/IndexController.getDataMaterialFromKodeProject')
    .middleware(['auth'])
  Route.post('pengeluaranMaterialSPM', 'API/v1/PengeluaranMaterial/IndexController.pengeluaranMaterialSPM')
    .middleware(['auth'])
}).prefix('api/v1')
