'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DataDetailMaterialCusSchema extends Schema {
  up () {
    this.create('data_detail_material_cuses', (table) => {
      table.increments()
      table.integer('id_penerimaan')
      table.date('tgl_penerimaan').nullable()
      table.string('bentuk', 10).nullable()
      table.string('no_surat_jalan', 30).nullable()
      table.string('heat_no', 30).nullable()
      table.string('lot_no', 30).nullable()
      table.integer('id_mvir').nullable()
      table.string('mvir_no', 30).nullable()
      table.string('mill_cert_no').nullable()
      table.string('visual').nullable()
      table.string('a').nullable()
      table.string('h').nullable()
      table.string('r').nullable()
      table.string('lokasi', 20).nullable()
      table.integer('urutan').nullable()
      table.string('status_kondisi').default('BELUM TERPAKAI')
      table.string('status_fisik').default('UTUH')
      table.timestamps()
    })
  }

  down () {
    this.drop('data_detail_material_cuses')
  }
}

module.exports = DataDetailMaterialCusSchema
