'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DataDetailMaterialPosSchema extends Schema {
  up() {
    this.create('data_detail_material_pos', (table) => {
      table.increments()
      table.integer('id_penerimaan')
      table.date('tgl_penerimaan').nullable()
      table.string('bentuk', 10).nullable()
      table.string('no_surat_jalan', 30).nullable()
      table.string('heat_no', 30).nullable()
      table.string('lot_no', 30).nullable()
      table.string('mvir_no', 30).nullable()
      table.string('lokasi', 20).nullable()
      table.integer('urutan').nullable()
      table.string('status_kondisi').default('BELUM TERPAKAI')
      table.string('status_fisik').default('UTUH')
      table.timestamps()
    })
  }

  down() {
    this.drop('data_detail_material_pos')
  }
}

module.exports = DataDetailMaterialPosSchema
