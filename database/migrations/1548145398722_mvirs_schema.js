'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MvirsSchema extends Schema {
  up () {
    this.table('mvirs', (table) => {
      table.string('mvir_from').after('type')
    })
  }

  down () {
    this.table('mvirs', (table) => {
      // reverse alternations
    })
  }
}

module.exports = MvirsSchema
