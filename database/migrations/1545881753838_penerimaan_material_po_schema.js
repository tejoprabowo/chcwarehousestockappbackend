'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PenerimaanMaterialPoSchema extends Schema {
  up() {
    this.create('penerimaan_material_pos', (table) => {
      table.increments()
      table.string('no_po', 15)
      table.string('kode_proyek')
      table.string('nama_barang', 30)
      table.string('spesifikasi', 90)
      table.string('satuan', 15)
      table.string('grade', 10)
      table.integer('h').nullable()
      table.integer('b').nullable()
      table.integer('t1').nullable()
      table.integer('t2').nullable()
      table.integer('length').nullable()
      table.integer('uw').nullable()
      table.integer('tonage').nullable()
      table.integer('qty').default(1)
      table.string('status_revisi_terima', 5).default('N')
      table.timestamps()
    })
  }

  down() {
    this.drop('penerimaan_material_pos')
  }
}

module.exports = PenerimaanMaterialPoSchema
