'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PengeluaranMaterialsSchema extends Schema {
  up() {
    this.create('pengeluaran_materials', (table) => {
      table.increments()
      table.integer('id_penerimaan')
      table.string('no_spm')
      table.date('tgl_spm')
      table.string('alokasi_material')
      table.string('type_material')
      table.string('keterangan').nullable()
      table.string('status')
      table.timestamps()
    })
  }

  down() {
    this.drop('pengeluaran_materials')
  }
}

module.exports = PengeluaranMaterialsSchema
