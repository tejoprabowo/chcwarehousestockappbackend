'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DataDetailMaterialPosSchema extends Schema {
  up() {
    this.table('data_detail_material_pos', (table) => {
      table.integer('id_mvir').after('lot_no')
    })
  }

  down() {
    this.table('data_detail_material_pos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = DataDetailMaterialPosSchema
