'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DataDetailMaterialPosSchema extends Schema {
  up () {
    this.table('data_detail_material_pos', (table) => {
      table.string('mill_cert_no').nullable().after('mvir_no')
      table.string('visual').nullable().after('mill_cert_no')
      table.string('a').nullable().after('visual')
      table.string('h').nullable().after('a')
      table.string('r').nullable().after('h')
    })
  }

  down () {
    this.table('data_detail_material_pos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = DataDetailMaterialPosSchema
