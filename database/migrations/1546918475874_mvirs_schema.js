'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MvirsSchema extends Schema {
  up () {
    this.create('mvirs', (table) => {
      table.increments()
      table.string('project')
      table.string('owner')
      table.string('main_cont')
      table.string('sub_cont')
      table.string('report_no').unique()
      table.date('date_mvir')
      table.string('description_of_goods')
      table.string('supplier_vendor')
      table.string('contract_po_no')
      table.string('type')
      table.timestamps()
    })
  }

  down () {
    this.drop('mvirs')
  }
}

module.exports = MvirsSchema
