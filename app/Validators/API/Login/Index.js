'use strict'

class APILoginIndex {
  get rules () {
    return {
      email: 'required',
      password: 'required'
    }
  }
}

module.exports = APILoginIndex
