'use strict'

class APIMasterProyekIndex {
  get rules () {
    return {
      namaProyek: 'required|unique:master_proyeks,nama_proyek',
      kodeProyek: 'required|unique:master_proyeks,kode_proyek'
    }
  }

  get messages () {
    return {
      'namaProyek.required': 'Nama proyek tidak boleh kosong',
      'namaProyek.unique': 'Nama proyek sudah pernah di pakai sebelumnya, ganti dengan nama proyek lain',
      'kodeProyek.required': 'Kode proyek tidak boleh kosong',
      'kodeProyek.unique': 'Kode proyek sudah pernah di pakai sebelumnya, ganti dengan kode proyek lain'
    }
  }
}

module.exports = APIMasterProyekIndex
