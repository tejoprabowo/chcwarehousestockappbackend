'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PenerimaanMaterialPo extends Model {

  static get dates() {
    return super.dates.concat(['tgl_penerimaan'])
  }

  static formatDates(field, value) {
    if (field === 'tgl_penerimaan') {
      return value.format('YYYY-MM-DD')
    }
    return super.formatDates(field, value)
  }

  static castDates(field, value) {
    if (field === 'tgl_penerimaan') {
      return value.format('YYYY-MM-DD')
    }
    return super.formatDates(field, value)
  }

}

module.exports = PenerimaanMaterialPo
