'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Mvir extends Model {

  static get dates() {
    return super.dates.concat(['date_mvir'])
  }

  // static formatDates(field, value) {
  //   if (field === 'date_mvir') {
  //     return value.format('YYYY-MM-DD')
  //   }
  //   return super.formatDates(field, value)
  // }

  static castDates(field, value) {
    if (field === 'date_mvir') {
      return value.format('YYYY-MM-DD')
    }
    return super.formatDates(field, value)
  }

}

module.exports = Mvir
