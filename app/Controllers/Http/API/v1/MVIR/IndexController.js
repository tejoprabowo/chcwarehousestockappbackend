'use strict'
const Mvir = use('App/Models/Mvir')
const DataDetailMaterialPo = use('App/Models/DataDetailMaterialPo')
const DataDetailMaterialCus = use('App/Models/DataDetailMaterialCus')

class IndexController {

  async getAllMvir({
    response
  }) {
    const mvir = await Mvir.all()
    response.status(200).send(mvir)
  }

  async saveNewMvir({
    request,
    response
  }) {
    const {
      project,
      owner,
      mainCont,
      subCont,
      reportNo,
      dateMvir,
      descriptionOfGoods,
      supplierVendor,
      contracPoNo,
      type,
      mvirFrom
    } = request.all()

    const createNewMvir = new Mvir()
    createNewMvir.project = project
    createNewMvir.owner = owner
    createNewMvir.main_cont = mainCont
    createNewMvir.sub_cont = subCont
    createNewMvir.report_no = reportNo
    createNewMvir.date_mvir = dateMvir
    createNewMvir.description_of_goods = descriptionOfGoods
    createNewMvir.supplier_vendor = supplierVendor
    createNewMvir.contract_po_no = contracPoNo
    createNewMvir.type = type
    createNewMvir.mvir_from = mvirFrom
    await createNewMvir.save()

    response.status(200).send('MVIR baru berhasil dibuat!')
  }

  async getDataMaterialByMvir({
    params,
    response
  }) {
    const dataDetailMaterialPo = await DataDetailMaterialPo.query()
      .leftJoin('penerimaan_material_pos', 'data_detail_material_pos.id_penerimaan', '=', 'penerimaan_material_pos.id')
      .leftJoin('mvirs', 'data_detail_material_pos.id_mvir', '=', 'mvirs.id')
      .where('data_detail_material_pos.id_mvir', params.idMvir)
      .select('data_detail_material_pos.*', 'penerimaan_material_pos.grade', 'penerimaan_material_pos.spesifikasi', 'penerimaan_material_pos.qty', 'penerimaan_material_pos.tonage', 'mvirs.mvir_from')
      .fetch()

    let checkJsonMaterialPo = await DataDetailMaterialPo.query()
      .leftJoin('penerimaan_material_pos', 'data_detail_material_pos.id_penerimaan', '=', 'penerimaan_material_pos.id')
      .where('data_detail_material_pos.id_mvir', params.idMvir)
      .select('data_detail_material_pos.*', 'penerimaan_material_pos.grade', 'penerimaan_material_pos.spesifikasi', 'penerimaan_material_pos.qty', 'penerimaan_material_pos.tonage')
      .getCount()
    if (checkJsonMaterialPo === 0) {
      const dataDetailMaterialCus = await DataDetailMaterialCus.query()
        .leftJoin('penerimaan_material_cuses', 'data_detail_material_cuses.id_penerimaan', '=', 'penerimaan_material_cuses.id')
        .leftJoin('mvirs', 'data_detail_material_cuses.id_mvir', '=', 'mvirs.id')
        .where('data_detail_material_cuses.id_mvir', params.idMvir)
        .select('data_detail_material_cuses.*', 'penerimaan_material_cuses.grade', 'penerimaan_material_cuses.spesifikasi', 'penerimaan_material_cuses.qty', 'penerimaan_material_cuses.tonage', 'mvirs.mvir_from')
        .fetch()

      return response.status(200).send(dataDetailMaterialCus)
    }
    response.status(200).send(dataDetailMaterialPo)
  }

  async updateDataMaterial({
    request,
    response
  }) {
    const {
      millCertNo,
      visual,
      statusAHR,
      idPenerimaan,
      mvirFrom
    } = request.all()

    if (mvirFrom === 'po') {
      await DataDetailMaterialPo.query()
        .where('id_penerimaan', idPenerimaan)
        .update({
          mill_cert_no: millCertNo,
          visual: visual,
          a: statusAHR,
          h: statusAHR,
          r: statusAHR
        })
    } else {
      await DataDetailMaterialCus.query()
        .where('id_penerimaan', idPenerimaan)
        .update({
          mill_cert_no: millCertNo,
          visual: visual,
          a: statusAHR,
          h: statusAHR,
          r: statusAHR
        })
    }

    response.status(200).send('Proses berhasil!')

  }

}

module.exports = IndexController
