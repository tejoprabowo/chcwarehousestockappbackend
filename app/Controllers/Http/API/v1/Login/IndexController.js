'use strict'
const User = use('App/Models/User')
const Hash = use('Hash')

class IndexController {

    async login ({ request, response, auth }) {
        const { email, password } = request.all()

        // Code di bawah ini memeriksa apakah user login menggunakan email atau id karyawannya
        const dataUser = await User.query().where('email', email).first() || await User.query().where('id_employee', email).first()

        // Code di bawah ini merespon jika email atau id employee tidak ada di database
        if (!dataUser) {
            return response.status(401).send({
              typeError: 'email',
              titleErr: 'Email/ID karyawan tidak di temukan!',
              textContentErr: 'Data email/ID karyawan anda tidak ada di database kami, mohon periksa kembali data anda dan silahkan coba kembali dengan teliti.'
            })
        }
        
        // Code di bawah ini memeriksa password user
        const checkPass = await Hash.verify(password, dataUser.password)
        if (!checkPass) {
            return response.status(401).send({
              typeError: 'password',
              titleErr: 'Password salah!',
              textContentErr: 'Password yang anda masukan salah. klik tombol lupa password untuk mereset ulang password jika anda lupa atau silahkan coba lagi dengan teliti.' 
            })
        }

        // Di bawah ini kode untuk mendapatkan token accesss
        const token = await auth.attempt(dataUser.email, password)
        response.status(200).send({
            dataToken: token,
            dataUser
        })
    }

}

module.exports = IndexController
