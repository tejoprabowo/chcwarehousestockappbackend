'use strict'
const PenerimaanMaterialPo = use('App/Models/PenerimaanMaterialPo')
const DataDetailMaterialPo = use('App/Models/DataDetailMaterialPo')
const Mvir = use('App/Models/Mvir')

class IndexController {

  async checkCountPenerimaanMaterial({
    request,
    response
  }) {
    const {
      dataMaterialPO
    } = request.all()
    const countMaterialPo = await PenerimaanMaterialPo.query()
      .where({
        no_po: dataMaterialPO.No_Po,
        spesifikasi: dataMaterialPO.Spesifikasi
      })
      .getCount()

    if (!countMaterialPo) {
      let i, iMax
      for (i = 0, iMax = dataMaterialPO.Qty; i < iMax; i++) {
        const insertNewPenerimaanMaterialPo = new PenerimaanMaterialPo()
        insertNewPenerimaanMaterialPo.no_po = dataMaterialPO.No_Po
        insertNewPenerimaanMaterialPo.kode_proyek = dataMaterialPO.Kd_Proyek
        insertNewPenerimaanMaterialPo.nama_barang = dataMaterialPO.Nm_Barang
        insertNewPenerimaanMaterialPo.spesifikasi = dataMaterialPO.Spesifikasi
        insertNewPenerimaanMaterialPo.satuan = dataMaterialPO.Kd_Satuan
        insertNewPenerimaanMaterialPo.grade = dataMaterialPO.Nm_Grade
        insertNewPenerimaanMaterialPo.h = dataMaterialPO.H || null
        insertNewPenerimaanMaterialPo.b = dataMaterialPO.B || null
        insertNewPenerimaanMaterialPo.t1 = dataMaterialPO.T1 || null
        insertNewPenerimaanMaterialPo.t2 = dataMaterialPO.T2 || null
        insertNewPenerimaanMaterialPo.length = dataMaterialPO.L || null
        insertNewPenerimaanMaterialPo.uw = dataMaterialPO.Unit_Weight || null
        insertNewPenerimaanMaterialPo.tonage = parseInt(dataMaterialPO.Unit_Weight) * 1
        await insertNewPenerimaanMaterialPo.save()
      }
    }

    const getDataMaterialPo = await PenerimaanMaterialPo.query()
      .leftJoin('data_detail_material_pos', 'penerimaan_material_pos.id', 'data_detail_material_pos.id_penerimaan')
      .where('penerimaan_material_pos.no_po', dataMaterialPO.No_Po)
      .where('penerimaan_material_pos.spesifikasi', dataMaterialPO.Spesifikasi)
      .select(
        'penerimaan_material_pos.id',
        'penerimaan_material_pos.no_po',
        'penerimaan_material_pos.kode_proyek',
        'penerimaan_material_pos.nama_barang',
        'penerimaan_material_pos.spesifikasi',
        'penerimaan_material_pos.satuan',
        'penerimaan_material_pos.grade',
        'penerimaan_material_pos.h',
        'penerimaan_material_pos.b',
        'penerimaan_material_pos.t1',
        'penerimaan_material_pos.t2',
        'penerimaan_material_pos.length',
        'penerimaan_material_pos.uw',
        'penerimaan_material_pos.qty',
        'data_detail_material_pos.id_penerimaan',
        'data_detail_material_pos.tgl_penerimaan',
        'data_detail_material_pos.bentuk',
        'data_detail_material_pos.no_surat_jalan',
        'data_detail_material_pos.heat_no',
        'data_detail_material_pos.lot_no',
        'data_detail_material_pos.mvir_no',
        'data_detail_material_pos.lokasi',
        'data_detail_material_pos.urutan'
      )
      .fetch()

    response.status(200).send(getDataMaterialPo)
  }

  async saveDataDetailMaterialPo({
    request,
    response
  }) {
    const {
      tglPenerimaan,
      noSuratJalan,
      mvirNo,
      noUrutMvir,
      lotNo,
      heatNo,
      lokasi,
      urutan,
      idPenerimaan
    } = request.all()

    // kode x001
    // Bagian script di bawah ini khusus untuk cel mvir apakah sudah di pakai atau belum
    const checkMvir = await DataDetailMaterialPo.query()
      .where('mvir_no', mvirNo + '-' + noUrutMvir)
      .first()
    const dataDetailMaterialPo = await DataDetailMaterialPo.query()
      .where('id_penerimaan', idPenerimaan)
      .first()
    if (checkMvir) {
      if (dataDetailMaterialPo) {
        if (checkMvir.id_penerimaan != dataDetailMaterialPo.id_penerimaan) {
          return response.status(409).send({
            error: true,
            msg: 'No MVIR sudah pernah di pakai sebelumnya!'
          })
        }
      } else {
        return response.status(409).send({
          error: true,
          msg: 'No MVIR sudah pernah di pakai sebelumnya!'
        })
      }
    }
    // End section kode x001

    // Kode x002
    // Kode di bawah ini untuk validasi apakah urutan dan lokasi material sudah di pakai atau belum
    const checkUrutan = await DataDetailMaterialPo.query()
      .leftJoin('penerimaan_material_pos', 'data_detail_material_pos.id_penerimaan', 'penerimaan_material_pos.id')
      .where({
        'data_detail_material_pos.lokasi': lokasi,
        'data_detail_material_pos.urutan': urutan
      })
      .select('penerimaan_material_pos.nama_barang', 'penerimaan_material_pos.spesifikasi', 'data_detail_material_pos.*')
      .first()
    if (checkUrutan) {
      return response.status(409).send({
        error: true,
        msg: 'Lokasi dan urutan sudah di pakai material ' + checkUrutan.nama_barang + ' spesifikasi ' + checkUrutan.spesifikasi
      })
    }
    // End Kode x002

    const getMvir = await Mvir.query()
      .where('report_no', mvirNo)
      .first()

    if (!dataDetailMaterialPo) {
      const insertDataDetailMaterialPo = await new DataDetailMaterialPo()
      insertDataDetailMaterialPo.id_penerimaan = idPenerimaan
      insertDataDetailMaterialPo.tgl_penerimaan = tglPenerimaan
      insertDataDetailMaterialPo.no_surat_jalan = noSuratJalan
      insertDataDetailMaterialPo.heat_no = heatNo
      insertDataDetailMaterialPo.lot_no = lotNo
      insertDataDetailMaterialPo.id_mvir = getMvir.id
      insertDataDetailMaterialPo.mvir_no = mvirNo + '-' + noUrutMvir
      insertDataDetailMaterialPo.lokasi = lokasi
      insertDataDetailMaterialPo.urutan = urutan
      await insertDataDetailMaterialPo.save()
    } else {
      await DataDetailMaterialPo.query()
        .where('id_penerimaan', idPenerimaan)
        .update({
          tgl_penerimaan: tglPenerimaan,
          no_surat_jalan: noSuratJalan,
          heat_no: heatNo,
          lot_no: lotNo,
          mvir_no: mvirNo + '-' + noUrutMvir,
          lokasi: lokasi,
          urutan: urutan
        })
    }

    response.status(200).send('Proses telah berhasil!')
  }

}

module.exports = IndexController
