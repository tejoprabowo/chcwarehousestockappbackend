'use strict'
const PenerimaanMaterialCus = use('App/Models/PenerimaanMaterialCus')
const DataDetailMaterialCus = use('App/Models/DataDetailMaterialCus')
const Mvir = use('App/Models/Mvir')

class IndexController {

  async savePenerimaanMaterialCus({
    request,
    response
  }) {
    const {
      noOrder,
      kodeProject,
      dataMaterial
    } = request.all()

    const checkMaterialCus = await PenerimaanMaterialCus.query()
      .where({
        no_order: noOrder,
        kode_proyek: kodeProject,
        spesifikasi: dataMaterial.spesifikasi
      })
      .first()

    if (checkMaterialCus) {
      return response.status(200).send({
        status: 'material already exist',
        msg: 'Gagal input di karnakan data material ' + dataMaterial.nama_barang + ' spesifikasi ' + dataMaterial.spesifikasi + ' sudah pernah di input sebelumnya untuk project ' + kodeProject + '!'
      })
    }

    let i, iMax
    for (i = 0, iMax = parseInt(dataMaterial.qty); i < iMax; i++) {
      const penerimaanMaterialCus = new PenerimaanMaterialCus()
      penerimaanMaterialCus.no_order = noOrder
      penerimaanMaterialCus.kode_proyek = kodeProject
      penerimaanMaterialCus.nama_barang = dataMaterial['nama_barang']
      penerimaanMaterialCus.spesifikasi = dataMaterial['spesifikasi']
      penerimaanMaterialCus.satuan = dataMaterial['satuan']
      penerimaanMaterialCus.grade = dataMaterial['grade']
      penerimaanMaterialCus.h = dataMaterial['h']
      penerimaanMaterialCus.b = dataMaterial['b']
      penerimaanMaterialCus.t1 = dataMaterial['t1']
      penerimaanMaterialCus.t2 = dataMaterial['t2']
      penerimaanMaterialCus.length = dataMaterial['length']
      penerimaanMaterialCus.uw = dataMaterial['uw']
      penerimaanMaterialCus.tonage = dataMaterial['uw']
      penerimaanMaterialCus.qty = 1
      await penerimaanMaterialCus.save()
    }
    response.status(200).send({
      status: 'Material success save',
      msg: 'Sukses save material ' + dataMaterial.nama_barang + ' spesifikasi ' + dataMaterial.spesifikasi + ' untuk project ' + kodeProject + '!'
    })
  }

  async getPenerimaanMaterialCus({
    request,
    response
  }) {
    const {
      noOrder
    } = request.all()
    const penerimaanMaterialCus = await PenerimaanMaterialCus.query()
      .leftJoin('data_detail_material_cuses', 'penerimaan_material_cuses.id', 'data_detail_material_cuses.id_penerimaan')
      .where('penerimaan_material_cuses.no_order', noOrder)
      .select('penerimaan_material_cuses.*', 'data_detail_material_cuses.mvir_no', 'data_detail_material_cuses.id_penerimaan', 'data_detail_material_cuses.tgl_penerimaan', 'data_detail_material_cuses.no_surat_jalan', 'data_detail_material_cuses.heat_no', 'data_detail_material_cuses.lot_no', 'data_detail_material_cuses.mvir_no', 'data_detail_material_cuses.lokasi', 'data_detail_material_cuses.urutan')
      .fetch()

    response.status(200).send(penerimaanMaterialCus)
  }

  async saveDataDetailMaterialCus({
    request,
    response
  }) {
    const {
      tglPenerimaan,
      noSuratJalan,
      mvirNo,
      noUrutMvir,
      lotNo,
      heatNo,
      lokasi,
      urutan,
      idPenerimaan
    } = request.all()

    const checkMvir = await DataDetailMaterialCus.query()
      .where('mvir_no', mvirNo + '-' + noUrutMvir)
      .first()

    const dataDetailMaterialCus = await DataDetailMaterialCus.query()
      .where('id_penerimaan', idPenerimaan)
      .first()

    if (checkMvir && !dataDetailMaterialCus) {
      return response.status(409).send({
        errorType: 'mvir error',
        msg: 'No MVIR sudah pernah di pakai sebelumnya!'
      })
    }

    const getMvir = await Mvir.query()
      .where('report_no', mvirNo)
      .first()

    if (!dataDetailMaterialCus) {
      const insertDataDetailMaterialCus = await new DataDetailMaterialCus()
      insertDataDetailMaterialCus.id_penerimaan = idPenerimaan
      insertDataDetailMaterialCus.tgl_penerimaan = tglPenerimaan
      insertDataDetailMaterialCus.no_surat_jalan = noSuratJalan
      insertDataDetailMaterialCus.heat_no = heatNo
      insertDataDetailMaterialCus.lot_no = lotNo
      insertDataDetailMaterialCus.id_mvir = getMvir.id
      insertDataDetailMaterialCus.mvir_no = mvirNo + '-' + noUrutMvir
      insertDataDetailMaterialCus.lokasi = lokasi
      insertDataDetailMaterialCus.urutan = urutan
      await insertDataDetailMaterialCus.save()
    } else {
      await DataDetailMaterialCus.query()
        .where('id_penerimaan', idPenerimaan)
        .update({
          tgl_penerimaan: tglPenerimaan,
          no_surat_jalan: noSuratJalan,
          heat_no: heatNo,
          lot_no: lotNo,
          mvir_no: mvirNo + '-' + noUrutMvir,
          lokasi: lokasi,
          urutan: urutan
        })
    }

    response.status(200).send('Proses telah berhasil!')
  }

}

module.exports = IndexController
