'use strict'

class IndexController {

    async checkToken ({ response, auth }) {
        try {
            const checkToken = await auth.check()
            if (checkToken) {
                const dataUser = await auth.getUser()
                response.status(200).send(dataUser)
            }
        } catch (e) {
            response.status(400).send('Sistem gagal/error mencari kecocokan token')
        }
    }

}

module.exports = IndexController
