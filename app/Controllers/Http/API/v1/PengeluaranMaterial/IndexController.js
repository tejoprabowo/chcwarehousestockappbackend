'use strict'
const PenerimaanMaterialPo = use('App/Models/PenerimaanMaterialPo')
const PenerimaanMaterialCus = use('App/Models/PenerimaanMaterialCus')
const PengeluaranMaterial = use('App/Models/PengeluaranMaterial')
const DataDetailMaterialPo = use('App/Models/DataDetailMaterialPo')
const DataDetailMaterialCus = use('App/Models/DataDetailMaterialCus')

class IndexController {

  async getDataMaterialFromKodeProject({
    request,
    response
  }) {
    const {
      kodeProject,
      typeMaterial
    } = request.all()
    let dataMaterial = []
    if (typeMaterial === 'Material PO') {
      dataMaterial = await PenerimaanMaterialPo.query()
        .leftJoin('data_detail_material_pos', 'penerimaan_material_pos.id', 'data_detail_material_pos.id_penerimaan')
        .where({
          'penerimaan_material_pos.kode_proyek': kodeProject,
          'data_detail_material_pos.status_kondisi': 'BELUM TERPAKAI'
        })
        .whereNotNull('data_detail_material_pos.mvir_no')
        .select('data_detail_material_pos.*', 'penerimaan_material_pos.nama_barang', 'penerimaan_material_pos.spesifikasi')
        .fetch()
    } else if (typeMaterial === 'Material Customer') {
      dataMaterial = await PenerimaanMaterialCus.query()
        .leftJoin('data_detail_material_cuses', 'penerimaan_material_cuses.id', 'data_detail_material_cuses.id_penerimaan')
        .where('penerimaan_material_cuses.kode_proyek', kodeProject)
        .fetch()
    }

    response.status(200).send(dataMaterial)
  }

  async pengeluaranMaterialSPM({
    request,
    response
  }) {
    const {
      noSpm,
      tglSpm,
      alokasiMaterial,
      typeMaterial,
      keterangan,
      dataMaterial
    } = request.all()
    for (let value of dataMaterial) {
      const pengeluaranMaterial = new PengeluaranMaterial()
      pengeluaranMaterial.id_penerimaan = value.id_penerimaan
      pengeluaranMaterial.no_spm = noSpm
      pengeluaranMaterial.tgl_spm = tglSpm
      pengeluaranMaterial.alokasi_material = alokasiMaterial
      pengeluaranMaterial.type_material = typeMaterial
      pengeluaranMaterial.keterangan = keterangan
      pengeluaranMaterial.status = 'PROSES PENGAMBILAN'
      if (await pengeluaranMaterial.save()) {
        if (typeMaterial === 'Material PO') {
          const dataDetailMaterialPo = await DataDetailMaterialPo.query()
            .where('id_penerimaan', value.id_penerimaan)
            .first()
          dataDetailMaterialPo.status_kondisi = 'PROSES PENGAMBILAN'
          await dataDetailMaterialPo.save()
        } else if (typeMaterial === 'Material Customer') {
          const dataDetailMaterialCus = await DataDetailMaterialCus.query()
            .where('id_penerimaan', value.id_penerimaan)
            .first()
          dataDetailMaterialCus.status_kondisi = 'PROSES PENGAMBILAN'
          await dataDetailMaterialCus.save()
        }
      }
    }

    response.status(200).send('Proses berhasil!')
  }

}

module.exports = IndexController
